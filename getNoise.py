import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.optimize as opt
import scipy.stats as stat


class Noise():
    def get_cshort(self, ys, cds):
        def f_1(x, K, B):
            return K * x + B

        assert len(ys) == len(cds)

        K, B = opt.curve_fit(f_1, ys, cds)[0]
        print(K, B)

        x1 = np.arange(0, 6, 0.01)
        y1 = K * x1 + B

        plt.figure()
        plt.scatter(ys, cds, 25, 'red')
        plt.plot(x1, y1, "b-")
        plt.xlabel('y')
        plt.ylabel('cd')
        plt.show()
    
        return K / 2.0
    
    def get_cshort_1(self, ys, cds):
        assert len(ys) == len(cds)

        slope, intercept, r_value, p_value, stderr = stat.linregress(ys, cds)
        K, B = slope, intercept
        print("%.15f, %.15f" % (K, B))

        x1 = np.arange(0, 6, 0.01)
        y1 = K * x1 + B

        plt.figure()
        plt.scatter(ys, cds, 25, 'red')
        plt.plot(x1, y1, "b-")
        plt.xlabel('y')
        plt.ylabel('cd')
        plt.show()
    
        return K / 2.0  

    def get_point(self, xa, xb):
        # 输入同一曝光下的两张图，输出真值和cd
        assert xa.shape == xb.shape
        assert xa.ndim == 2
        avg = (xa + xb) / 2.0
        y = np.median(avg)
        # print(avg, y)

        sub = xa - xb
        cd = np.var(sub)
        # print(sub, cd)
        return [y, cd]

    def get_cread(self, biasFrames):
        # 读出噪声的方差
        # 至少5张bias frames 的 方差再取平均
        assert biasFrames.ndim == 3
        h, w, n = biasFrames.shape
        vars_biasFrames = []
        for i in range(n):
            vars_biasFrames.append(np.var(biasFrames[:, :, i]))
        return sum(vars_biasFrames) / n


if __name__ == "__main__":
    noise = Noise()
    rng = np.random.default_rng(12345)
    # biasFrames = rng.integers(0, 2 ** 12, (4, 2, 5))
    # cread = noise.get_cread(biasFrames) # 至少5张bias frames 的 方差再取平均
    # print(biasFrames[1, 1, :], cread)

    # xa = rng.integers(0, 2 ** 12, (4, 2))
    # xb = rng.integers(0, 2 ** 12, (4, 2))
    # print(noise.get_point(xa, xb))


    ys = [1, 2, 3, 4, 5]
    cds = [1, 3, 8, 18, 36]
    noise.get_cshort(ys, cds)
    noise.get_cshort_1(ys, cds)

    print('sucess!')